# Contributing guidelines

When contributing to this repository, even though we do check all submissions please ensure that your submissions actually work.

In addition, make use of the description (long and short) space in the pull request as common courtesy to let us know in english what the change is and maybe why its a good idea to include it. Blank or unmeaningful pull request messages may be rejected.

## Pull Request guidelines

Make a fork of this project first. Then create a branch within your fork with the changes. That way if you're asked to make certain changes it all can be reviewed in the pull request.
